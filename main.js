// 1.Описать своими словами для чего вообще нужны функции в программировании.
// 2.Описать своими словами, зачем в функцию передавать аргумент.
//
// //-----------------ответ---------------------------

// 1.Ф-ции нужны для того что бы не повторять один и тот же код в программе, т.е. написав функцию в коде мы можем вызвать n-ое количество раз     в разных участках кода.
// 2. аргумент это как  переменная(набор переменных, может быть ф-ци) которые будут вычесляться в функции и возвращать результат(не всегда:))
//    Аргумент  еще нужен для того чтобы сделать функцию универсальной и можно было бы вызывать ее.

let firstNumber = +prompt("Введите первое число");
let mathOperation = prompt("Введите математическую операцию '+', '-', '*', '/'.");
let secondNumber = +prompt("Введите второе число");

while (!firstNumber || isNaN(+firstNumber) || firstNumber == ' ' || !secondNumber || isNaN(+secondNumber) || secondNumber == ' ') {
    firstNumber = +prompt("Введите первое число", firstNumber);
    secondNumber = +prompt("Введите второе число", secondNumber);
}

alert(mathFunctions(firstNumber, mathOperation, secondNumber));

function mathFunctions(one, oper, two) {
    let result;
    switch (oper) {
        case "+":
            result = one + two;
            break;
        case "-":
            result = one - two;
            break;
        case "/":
            result = one / two;
            break;
        case "*":
            result = one * two;
            break;
    }
    return result;
}
